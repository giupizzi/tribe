# Tribe.so 👋

## About us page

- Information architecture and story telling.
- Communicating the vision and the culture.
- Copywriting and tone of voice.
- Use of visuals.
- Overall look and feel.
